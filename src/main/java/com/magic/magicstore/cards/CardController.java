package com.magic.magicstore.cards;

import com.magic.magicstore.cards.model.Card;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequiredArgsConstructor
@RequestMapping("/card")
public class CardController {

    private final CardFeignClient cardService;

    @GetMapping("/{id}")
    public ResponseEntity<?> getCard(@PathVariable("id") Long id) {
        return ResponseEntity.ok(cardService.getCard(id));
    }

    @GetMapping
    public ResponseEntity<?> getAllCards() {
        return ResponseEntity.ok(cardService.getAllCards());
    }

    @PostMapping
    public ResponseEntity<?> saveCard(@RequestBody Card card) {
        Card newCard = cardService.saveCard(card);
        URI location = URI.create(String.format("/card/%s", newCard.getId()));
        return ResponseEntity.created(location).body(newCard);
    }

    @PutMapping()
    public ResponseEntity<?> updateCard(@RequestBody Card card) {
        cardService.updateCard(card);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<?> deleteCard(@RequestBody Card card) {
        cardService.deleteCard(card);
        return ResponseEntity.noContent().build();
    }
}
