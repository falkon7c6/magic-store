package com.magic.magicstore.cards;

import com.magic.magicstore.cards.model.Card;
import feign.FeignException;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "magic-store-card-service")
public interface CardFeignClient {

    @GetMapping("/card/{id}")
//    @CircuitBreaker(name= "magic-store-card-service", fallbackMethod="defaultCard")
    @Retry(name = "magic-store-card-service", fallbackMethod = "defaultCard")
    public Card getCard(@PathVariable("id") Long id);

    default Card defaultCard(Long id, FeignException e) {
        System.out.println("retry?");
        return new Card(id, "not found", "not found", "not found");
    }

    @GetMapping("/card")
    public List<Card> getAllCards();

    @PostMapping("/card")
    public Card saveCard(@RequestBody Card card);

    @PutMapping("/card")
    public Card updateCard(@RequestBody Card card);

    @DeleteMapping("/card")
    public void deleteCard(@RequestBody Card card);
}
